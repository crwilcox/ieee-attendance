﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IEEEAttendance
{
    /// <summary>
    /// Interaction logic for MemberCreation.xaml
    /// </summary>
    public partial class MemberCreation : Window
    {
        public MemberCreation()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Button was clicked to complete Member Creation.  Try to add new member to the db
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MemberCreationComplete_Click(object sender, RoutedEventArgs e)
        {
            Member mem = new Member();
            //Populate mem object...
            mem.email = memCreationEmailAddress.Text;
            mem.firstName = memCreationFirstName.Text;
            mem.lastName = memCreationLastName.Text;
            mem.phone = memCreationPhoneNumber.Text;
            mem.studentID = memCreationStudentID.Text;

            //Check to make sure fields are in a good fashion
            long numStudentID;
            bool result = long.TryParse(mem.studentID, out numStudentID);
            if (mem.studentID.Length != 10 || !result || numStudentID < 1000000000 || numStudentID > 9999999999)
            {
                //if the studentID cannot be converted to a 10 digit number, then it was entered incorrectly
                MessageBox.Show("Student ID Must be 10 numeric digits and no other characters");
            }
            else
            {
                //Add Member to DataBase
                bool succ = mem.addMember();
                if (!succ) MessageBox.Show("Failed to add new member to database");//member added successfully
                //Go Back to Entry Page
                this.Close();
            }



        }
    }
}
